// Aggregation in mongoDB is typically done in 2-3 steps. Each step is called a stage. 
// It is grouping together documents to arrive at an explicit result
db.fruits.aggregate([

    // $match -> used to match or get documents that satisfies the condition 
    // $match stage is similar to find(). Infact, you can even add query operators to expand the criteria.
    /*
        Syntax:
            { $match: {<field_name>: <value>}}
    */
    {$match: {
        $and: [
            {onSale: true},
            {price: {$gte: 40}}
        ]
    }},
    // $group -> allows us to group together documents and create an analysis out of the grouped documents

    // _id: in the group stage, essentially associates an id to our results.
    // _id: also determines the number of groups
    // _id: "$<field_name>" -> group documents based on the value of the indicated field
    /*
        $match - apple, kiwi, banana, dragon fruit, mango

        $group: _id: "$supplier"

        _id: Red Farms

        Apple: supplier - Red Farms
        Dragon Fruit: supplier - Red Farms

        _id: Green Farming

        Kiwi: supplier - Green Farming

        _id: Yellow Farms

        Banana: supplier - Yellow Farms
        Mango: supplier - Yellow Farms
    */

    // $sum -> used to add or total the values in a given field 

    /*
        $sum:"$stocks" -> sum of the values of stocks field of each item per group

        _id: Red Farms

        Apple: supplier - Red Farms: stocks - 20
        Dragon Fruit: supplier - Red Farms : stocks - 10

        $sum = 30

        _id: Green Farming

        Kiwi: supplier - Green Farming: stocks - 25

        $sum = 25

        _id: Yellow Farms

        Banana: supplier - Yellow Farms: stocks: 15
        Mango: supplier - Yellow Farms: stocks: 10

        $sum = 25
    */
    { $group: {_id:"$supplier", totalStocks: {$sum:"$stocks"}}}

])

// $avg -> gets the avg of the values of given field per group.

db.fruits.aggregate([

    {$match: {onSale: true}},
    {$group: {_id:"$supplier", avgStocks: {$avg:"$stocks"}}}

])

db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id:null, avgPrice: {$avg:"$price"}}}
])

db.fruits.aggregate([
    {$match: {supplier: "Red Farms Inc."}},
    {$group: {_id:"group1", avgPrice: {$avg:"$price"}}}
])

// $max -> will allow us to get the highest value out of all the values in a given field per group.

db.fruits.aggregate([

    {$match: {onSale: true}},
    {$group: {_id:"maxStockOnSale", maxStock: {$max:"$stocks"}}}

])

db.fruits.aggregate([

    {$match: {onSale: true}},
    {$group: {_id:"highestPrice", maxPrice: {$max:"$price"}}}

])

// $min -> will allow us to get the lowest value out of all the values in a given field per group.

db.fruits.aggregate([

    {$match: {onSale: true}},
    {$group: {_id:"lowestStockOnSale", minStock: {$min:"$stocks"}}}

])

db.fruits.aggregate([

    {$match: {supplier: "Yellow Farms"}},
    {$group: {_id:"lowestPrice", minPrice: {$min: "$price"}}}

])

// $count -> is a stage usually added after $match to count all items that matches our criteria.

db.fruits.aggregate([

    {$match: {onSale: true}},
    {$count: "itemsOnSale"}

])

db.fruits.aggregate([

    {$match: {price: {$gt: 50}}},
    {$count: "itemsPriceMoreThan50"}

])

db.fruits.aggregate([

    {$match: {
        $and: [
            {price: {$gt: 50}},
            {stocks: {$lt: 20}}
        ]
        }},
    {$count: "itemsPriceGreaterThan50below20sticks"}

])

// $out -> save/output the result in a new collection
// it is usually the last stage in an aggregation pipeline.
// Note: this will overwrite the collection if it already exists

db.fruits.aggregate([

    {$match: {onSale: true}},
    {$group: {_id: "$supplier", totalStocks:{$sum: "$stocks"}}},
    {$out: "stocksPerSupplier"}

])

db.fruits.aggregate([

    {$match: {price: {$lte: 50}}},
    {$group: {_id: "$supplier", totalStocks:{$sum: "$stocks"}}},
    {$out: "stocksPerSupplier"}

])

// [Mini Activity]

db.fruits.aggregate([

    {$match: {onSale: true}},
    {$group: {_id: "$supplier", minStocks:{$min: "$stocks"}}},
    {$out: "lowestNumberOfStock"}

])

